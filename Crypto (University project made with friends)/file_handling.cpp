//
// Created by gawenda on 13.06.18.
//

#include "file_handling.h"
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

std::locale loc;
std::stringstream stringstream;

//this function writes contents of a file into one string
std::string read_contents(std::string file_name) {
    std::ifstream file;

    std::string file_contents;
    bool open_success = false;

    while(!open_success){
        file.open(file_name, std::ios::in);

        if(!file){
            std::cout << "error, couldn't open the file " << file_name << std::endl;
            file.clear(file.rdstate() & ~std::ios::failbit);
        }
        else {
            open_success = true;
        }
        if (!open_success) {
            std::cout << "input file name (type 'quit' to cancel):";
            std::cin >> file_name;
        }
        if (file_name == "quit") break;

    }

    if (file_name == "quit"){
        return "none";
    }

    file.seekg(0, file.end);
    int length = static_cast<int>(file.tellg()) - 1;
    file.seekg(0, file.beg);

    auto *buffer = new char [length+1];
    file.read(buffer, length+1);

    file_contents = buffer;

    file.close();
    delete[] buffer;

    return file_contents;
}

//takes input from user
std::string get_text() {
    std::string text;
    std::getline(std::cin >> std::ws,  text, '\n');

    return text;
}

//removes every nonalphabetic character from the text
std::string remove_nonalpha(std::string text) {
    for (std::string::iterator it = text.begin(); it != text.end(); it++) {
        if(!isalpha(*it)) {
            text.erase(it);
            it--;
        }
    }
    return text;
}

//writes text into a file
void write_contents(std::string text, std::string file_name) {
    std::ofstream file (file_name);

    for (int i = 0; i < text.size(); i++) {
        file.put(text[i]);
    }

    //file.put('\n').put('\n');

    file.close();
}
