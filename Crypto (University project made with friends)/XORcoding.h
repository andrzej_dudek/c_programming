#ifndef XORCODING_LIBRARY_H
#define XORCODING_LIBRARY_H

#include <iostream>
#include <string>

using namespace std;

string XORcode(string source, char key);

string XORdecode(string coded, char key);

#endif