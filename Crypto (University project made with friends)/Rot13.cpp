#include "Rot13.h"

string Rot13code(string source) {
    string transformed;
    for (size_t i = 0; i < source.size(); ++i) {
        if (isalpha(source[i])) {
            if ((tolower(source[i]) - 'a') < 13)
                transformed.append(1, source[i] + 13);
            else
                transformed.append(1, source[i] - 13);
        }
        else {
            transformed.append(1, source[i]);
        }
    }
    return transformed;
}

string Rot13decode(string coded) {
    string transformed;
    for (size_t i = 0; i < coded.size(); ++i){
        if (isalpha(coded[i])){
            if((tolower(coded[i]) - 'a') < 13)
                transformed.append(1, coded[i] + 13);
            else
                transformed.append(1, coded[i] - 13);
        }
        else {
            transformed.append(1, coded[i]);
        }
    }
    return transformed;
}
