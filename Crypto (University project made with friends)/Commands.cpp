//
// Created by gawenda on 20.08.18.
//

#include "Commands.h"
#include "file_handling.h"
#include "Rot13.h"
#include "XORcoding.h"
#include "kod Cezara.h"
#include <string>
#include <iostream>
#include <iomanip>
#include <iterator>
#include <vector>

bool b_write_to_file{true}; //if parameter '-o' is present, it writes encoded/decoded information into a file
bool b_write_to_terminal{false};
// bool b_write_to_destination{false}; if parameter '-d' is present, it writes encoded/decoded information into a file (from path)

bool b_input_from_file{false}; //if parameter '-i' is present, it takes information to encode/decode from a file
bool b_input_from_user{true}; //if parameter '-i' is present, it gives a prompt to make user input his info to encode/decode

bool b_encoding{false}; //if '-en <type>' is present, the program is set to encode information, where <type> defines encoding method
bool b_decoding{false}; //if '-de <type>' is present, the program is set to decode information, where <type> defines decoding method

bool b_rot13{false};  //
bool b_xor{false};    //those three set encoding/decoding method for the program
bool b_caesar{false}; //

//bool b_binary{false}; if parameter '-b' is present, the program is set to write information in binary code. NOT USED

std::string output_en_name{"output_en.txt"};
std::string output_de_name{"output_de.txt"};
std::string input_name{"input.txt"};
char encoding_key{'x'};

std::string user_input{" "};


void parse_commands(std::vector<std::string> arguments) {
    for (int i = 0; i < arguments.size(); i++) {
        //checks for ENCODING parameters
        if (arguments[i] == "-en") {
            b_encoding = true;
            b_xor = true;
            if ((i + 1) < arguments.size()) {
                if (arguments[i + 1] == "rot13") {
                    b_rot13 = true;
                    b_xor = b_caesar;
                }
                else if (arguments[i + 1] == "caesar") {
                    b_caesar = true;
                    b_xor = b_rot13;
                }
            }
        }

        //checks for DECODING parameters
        if (arguments[i] == "-de") {
            b_decoding = true;
            if ((i + 1) < arguments.size()) {
                if (arguments[i + 1] == "rot13") {
                    b_rot13 = true;
                    b_xor = b_caesar;
                }
                else if (arguments[i + 1] == "caesar") {
                    b_caesar = true;
                    b_xor = b_rot13;
                }
                else if (arguments[i + 1] == "xor"){
                    b_xor = true;
                }
            }
        }

        //used for changing output file name.
        if (arguments[i] == "-o") {
            b_write_to_file = true;
            if ((i + 2) < arguments.size()) {
                if (arguments[i + 1] == "en"){
                    if (arguments[i + 2].front() != '-') {
                        if (arguments[i + 2].rfind(".txt") == std::string::npos) {
                            arguments[i + 2].append(".txt");
                            output_en_name = arguments[i + 2];
                        }
                        else {
                            output_en_name = arguments[i + 2];
                        }
                    }
                }
                else if (arguments[i + 1] == "de"){
                    if (arguments[i + 2].front() != '-') {
                        if (arguments[i + 2].rfind(".txt") == std::string::npos) {
                            arguments[i + 2].append(".txt");
                            output_de_name = arguments[i + 2];
                        }
                        else {
                            output_de_name = arguments[i + 2];
                        }
                    }
                }
            }
        }

        //used for changing input method. User input by default.
        if (arguments[i] == "-i"){
            if ((i + 1) < arguments.size()) {
                if (arguments[i + 1] == "user") {
                    b_input_from_user = true;
                }
                else if (arguments[i + 1] == "file") {
                    b_input_from_file = true;
                    b_input_from_user = false;
                    if (arguments[i + 2].front() != '-') {
                        if (arguments[i + 2].rfind(".txt") == std::string::npos) {
                            arguments[i + 2].append(".txt");
                            input_name = arguments[i + 2];
                        }
                        else {
                            input_name = arguments[i + 2];
                        }
                    }
                }
            }
        }

        /*if (arguments[i] == "-b") {
            b_binary = true;
        }*/
    }

    //std::cout << std::setiosflags(std::ios::boolalpha);

    /*std::cout << b_write_to_file << "\n" << b_input_from_file << " "
              << b_input_from_user << " " << std::endl
              << b_encoding << " " << b_decoding << " " << std::endl
              << b_rot13 << " " << b_xor << " " << b_caesar << std::endl
              << input_name;
    */
}

//enrypts text
std::string encode_text(std::string text) {
    if (b_encoding) {
        if (b_rot13) {
            return Rot13code(text);
        }
        else if (b_xor) {
            std::cout << "Please input your one character key:\n";
            std::cin.get(encoding_key);
            return XORcode(text, encoding_key);
        }
        else if (b_caesar) {
            return Cezar_en(text);
        }
    }
}

//decodes text
std::string decode_text(std::string text){
    if (b_decoding){
        if (b_rot13){
            return Rot13decode(text);
        }
        else if (b_xor){
            std::cout << "Please input your one character key:\n";
            std::cin.get(encoding_key);
            return XORdecode(text, encoding_key);
        }
        else if (b_caesar){
            return Cezar_de(text);
        }
    }
}

//gets text to decode/encrypt
std::string get_contents(){
    if (b_input_from_user){
        std::cout << "Please input your message here: ";
        return get_text();
    }
    else if (b_input_from_file){
        return read_contents(input_name);
    }
}

//saves text in a file
void save_text(){
    if (b_encoding || b_decoding){
        std::string text = get_contents();
        if (b_encoding) {
            std::string encoded_text = encode_text(text);
            write_contents(encoded_text, output_en_name);
        }

        if (b_decoding) {
            std::string decoded_text = decode_text(text);
            write_contents(decoded_text, output_de_name);
        }
    }
    else {
        std::cout << "NO METHOD SELECTED. USE '-en' OR '-de' TO ENABLE FUNCTIONALITY.\n";
    }
}

/*
 std::string get_input_name(){
    return input_name;
}

std::string get_output_name(){
    return output_name;
}

char get_encoding_key(){
    return encoding_key;
}
*/
