//
// Created by gawenda on 20.08.18.
//

#ifndef CRYPTO_COMMANDS_H
#define CRYPTO_COMMANDS_H

#include <string>
#include <vector>

void parse_commands(std::vector<std::string> arguments);
std::string encode_text(std::string text);
std::string decode_text(std::string text);
std::string get_contents();
void save_text();
// char get_encoding_key();
// std::string get_input_name();
// std::string get_output_name();



#endif //CRYPTO_COMMANDS_H
