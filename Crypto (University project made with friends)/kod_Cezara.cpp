#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

/*
	Utworzone przez Kacpra Szepielaka
*/

/*
string Cezar(string tekst, int klucz = 3)
{
    int i = 0;
    int ilosc_liter = 'z' - 'a' + 1;
    while( tekst[i] != '\0' )
    {
        tekst[i] += klucz;
        // w ponizszej linijce jest rzutowanie, dlatego ze wartosci w tekscie mogl wyjsc ujemne po dodaniu klucza (np 'z'+10)
        if((unsigned char)tekst[i] > 'z') tekst[i] -= ilosc_liter;

        if((unsigned char)tekst[i] < 'a' ) tekst[i] += ilosc_liter;

        i++;
    }
    return tekst;
}
*/
string Cezar_en(string tekst) {
    string zakodowany;
    for (int i = 0; i < tekst.size(); i++){
        if (tolower(tekst[i]) >= 'a' && tolower(tekst[i]) <= 'z') {
            if (tolower(tekst[i]) + 3 > 'z') zakodowany.push_back(tekst[i] - 23);
            else zakodowany.push_back(tekst[i] + 3);
        }
        else zakodowany.push_back(tekst[i]);
    }
    return zakodowany;
}

string Cezar_de(string tekst) {
    string odkodowany;
    for (int i = 0; i < tekst.size(); i++){
        if (tolower(tekst[i]) >= 'a' && tolower(tekst[i]) <= 'z') {
            if (tolower(tekst[i]) - 3 < 'a') odkodowany.push_back(tekst[i] + 23);
            else odkodowany.push_back(tekst[i] - 3);
        }
        else odkodowany.push_back(tekst[i]);
    }
    return odkodowany;
}
