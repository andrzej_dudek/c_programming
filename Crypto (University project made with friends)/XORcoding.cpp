#include "XORcoding.h"
string XORcode(string source, char key) {
    string transformed;
    for(size_t i = 0; i < source.size(); ++i){
        transformed.append(1, source[i] ^ key);

    }
    return transformed;
}

string XORdecode(string coded, char key) {
    string transformed;
    for(size_t i = 0; i < coded.size(); ++i){
        transformed.append(1, coded[i] ^ key);
    }
    return transformed;
}
