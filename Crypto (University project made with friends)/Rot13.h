#ifndef ROT13_LIBRARY_H
#define ROT13_LIBRARY_H

#include <iostream>
#include <string>

using namespace std;

/*
 * Function Rot13code is coding using the ROT13 algorithm
 * @param is a string to be coded
 * @return a coded string
 */
string Rot13code(string source);

/*
 * Function Rot13decode is decoding strings coded using the ROT13 algorithm
 * @param coded string
 * @return decoded string
 */
string Rot13decode(string coded);

#endif