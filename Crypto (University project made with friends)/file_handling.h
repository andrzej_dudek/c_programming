//
// Created by gawenda on 13.06.18.
//

#ifndef SZYFROWANIE_FILE_HANDLING_H
#define SZYFROWANIE_FILE_HANDLING_H

#include <string>

std::string read_contents(std::string filename); //reads text from file
std::string get_text(); //takes text from user's input and returns a string
std::string remove_nonalpha(std::string text); //removes whitespaces, symbols and numbers
void write_contents(std::string text, std::string file_name); //writes text to file


#endif //SZYFROWANIE_FILE_HANDLING_H
