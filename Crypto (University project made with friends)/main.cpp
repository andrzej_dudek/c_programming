#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include "Commands.h"

/* 
	Autorzy programu (studenci z pierwszego roku EiT, grupa 1):
	- Jakub Gawenda
	- Andrzej Dudek
	- Kacper Szepielak

	Program ten jest przeznaczony do uruchamiania w terminalu! Uruchomienie go bez żadnych parametrów wywoła wyświetlenie dostępnych opcji 
	i argumentów na ekranie.
*/

int main(int argc, char **argv){
    int main_gap = 10;
    int sec_gap = main_gap + 15;

    if (argc == 1 || argv[1] == "-h") {
        std::cout << "Welcome to Crypto encryption program! This program is meant to be used in a terminal/command line enviroment!\n"
                "To see this message again, please run the program without any arguments.\n\n";
        std::cout << "COMMANDS:\n";
        std::cout << std::setw(main_gap) << "" << "'-en <type>'" << "- enables encrypting and allows to choose encryption type.\n";
        std::cout << std::setw(sec_gap) << "'caesar'" << " - for Caesar cipher encrytpion\n";
        std::cout << std::setw(sec_gap) << "'rot13'" << " - for Rot13 cipher encryption\n";
        std::cout << std::setw(sec_gap) << "'xor'" << " - for Xor cipher encryption (default)\n\n";

        std::cout << std::setw(main_gap) << "" << "'-de <type>'" << "- enables decoding and allows to choose decoding type.\n";
        std::cout << std::setw(sec_gap) << "'caesar'" << " - for Caesar cipher decoding\n";
        std::cout << std::setw(sec_gap) << "'rot13'" << " - for Rot13 cipher decoding\n";
        std::cout << std::setw(sec_gap) << "'xor'" << " - for Xor cipher decoding (default)\n\n";

        std::cout << std::setw(main_gap) << "" <<"'-o <type> <name>'" << "- changes name of the output file.\n";
        std::cout << std::setw(sec_gap) << "'de'" << " - for changing decoder's output file\n";
        std::cout << std::setw(sec_gap) << "'en'" << " - for changing encoder's output file\n\n";

        std::cout << std::setw(main_gap) << "" << "'-i <type> <name/path>'" << "- changes input type.\n";
        std::cout << std::setw(sec_gap) << "'user'" << " - prompts user to input text to encrypt/decode\n";
        std::cout << std::setw(sec_gap) << "'file'" << " - takes input from <name> file. In case of errors, defaults to input.txt.\n\n";

        std::cout << std::setw(main_gap) << "";
    }


    std::vector<std::string> arguments;

    for (int i = 1; i < argc; i++){
        arguments.push_back(argv[i]);
    }

    parse_commands(arguments);

    if (argc > 1) {
        save_text();
    }
    return 0;
}