//
// Created by gawenda on 29.08.18.
//

#ifndef CRYPTO_KOD_CEZARA_H
#define CRYPTO_KOD_CEZARA_H

#include <string>

//std::string Cezar(std::string tekst, int klucz = 3);
std::string Cezar_en(std::string tekst);
std::string Cezar_de(std::string tekst);

#endif //CRYPTO_KOD_CEZARA_H
