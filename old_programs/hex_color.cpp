#include <iostream>
#include <cmath>

// define alphabet for hexadecimal
std::string hex_alphabet = "01234567890ABCDEF";

int* hex_rgb_color(float number);

void hex_color(float red, float green, float blue);

int main() {

    float red, green, blue;
    std::cout << "Podaj wartości RGB\nR = ";
    std::cin >> red;
    std::cout << "G = ";
    std::cin >> green;
    std::cout << "B = ";
    std::cin >> blue;

    hex_color(red, green, blue);

    return 0;
}

// this function convetrs a number from 0-255 range into a table with value of 
// 2 hexadecimal digits and returns a pointer to that table
int* hex_rgb_color(float number) {

    if (number < 1) {
        number = number * 255;
    }

    number = number / 16;
    int num_int = (int) number;
    float num_float = number - num_int;
    int tab[2];
    num_float = num_float * 255 / 16;
    tab[0] = num_int;
    tab[1] = round(num_float);
    int* ptr = tab;

    return ptr;
};

// this function writes a color in hexadecimal rgb standard
// it converts values from table to a hexadecimal value
// and writes them in a color string
void hex_color(float red, float green, float blue) {

    std::string color = "#000000";

    int* color_ptr = hex_rgb_color(red);
    color[1] = hex_alphabet[*color_ptr];
    color[2] = hex_alphabet[*(color_ptr + 1)];

    color_ptr = hex_rgb_color(green);
    color[3] = hex_alphabet[*color_ptr];
    color[4] = hex_alphabet[*(color_ptr + 1)];

    color_ptr = hex_rgb_color(blue);
    color[5] = hex_alphabet[*color_ptr];
    color[6] = hex_alphabet[*(color_ptr + 1)];

    std::cout << "Kolor (" << red << ", " << green << ", " << blue << ") to " << color << "\n";

};