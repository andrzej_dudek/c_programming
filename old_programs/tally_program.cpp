#include <iostream>

int SCORE_TABLE[5];
char SCORE_INDEX[] = {'a', 'b', 'c', 'd', 'e'};

///
/// \param position - position in SCORE_TABLE
///
void increment(int position){
    SCORE_TABLE[position]++;
}

///
/// \param position - position in SCORE_TABLE
///
void decrement(int position){
    SCORE_TABLE[position]--;
}

///
/// \param letter - character from the word
/// \return position for the table
///
int get_position(char letter){
    int position;
    if (isupper(letter)){
        position = (int) letter - (int) 'A';
    }
    else {
        position = (int) letter - (int) 'a';
    }
    return position;
}

///
/// \param word - some input string
///
void string_analyzer(std::string word){
    int position;

    for (char i : word) {
        if (isalpha(i)){
            position = get_position(i);

            if (position > 4) continue;

            if(isupper(i)){
                decrement(position);
            }
            else{
                increment(position);
            }
        }
        else continue;
    }
}

void display_and_reset(){
    std::cout << SCORE_INDEX[0] << ": " << SCORE_TABLE[0]
              << "\t" << SCORE_INDEX[1] << ": " << SCORE_TABLE[1]
              << "\t" << SCORE_INDEX[2] << ": " << SCORE_TABLE[2]
              << "\t" << SCORE_INDEX[3] << ": " << SCORE_TABLE[3]
              << "\t" << SCORE_INDEX[4] << ": " << SCORE_TABLE[4] << "\n";

    for (int i = 0; i < 5; i++){
        SCORE_TABLE[i] = 0;
        SCORE_INDEX[i] = (char) ((int) 'a' + i);
    }
}

void sort(){

    int num_hide;
    char char_hide;
    bool flag = true;

    do{
        flag = false;

        for (int position = 0; position < 4; position++) {
            if (SCORE_TABLE[position + 1] > SCORE_TABLE[position]) {
                flag = true;
                num_hide = SCORE_TABLE[position];
                char_hide = SCORE_INDEX[position];
                SCORE_TABLE[position] = SCORE_TABLE[position + 1];
                SCORE_INDEX[position] = SCORE_INDEX[position + 1];
                SCORE_TABLE[position + 1] = num_hide;
                SCORE_INDEX[position + 1] = char_hide;
            }
        }
    } while (flag);
}

int main() {
    std::cout << "Testing:\n";

    std::cout << "abcde\n";
    string_analyzer("abcde\n");
    sort();
    display_and_reset();

    std::cout << "dbbaCEDbdAacCEAadcB\n";
    string_analyzer("dbbaCEDbdAacCEAadcB\n");
    sort();
    display_and_reset();

    std::cout << "EbAAdbBEaBaaBBdAccbeebaec\n";
    string_analyzer("EbAAdbBEaBaaBBdAccbeebaec\n");
    sort();
    display_and_reset();

    return 0;
}