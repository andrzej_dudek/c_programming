#include <iostream>

// this function remove spaces
std::string remove_spaces(std::string sentence){
    int sentence_length = sentence.length();
    std::string return_string = "";

    for(int i = 0; i<sentence_length; i++){
        if (sentence[i] != ' '){
            return_string += sentence[i];
        }
        else continue;
    }

    return return_string;
}

// this function replaces ',' with '.'
std::string replace_comas(std::string sentence){
    int sentence_length = sentence.length();
    std::string return_string = "";

    for(int i = 0; i<sentence_length; i++){
        if (sentence[i] != ','){
            return_string += '.';
        }
        else {
            return_string += sentence[i];
        }
    }

    return return_string;
}

// this function tests if there is '.' in a string
// if there is returns true otherwise returns false
bool test_coma(std::string param){
    std::size_t found = param.find('.', 0);
    if(found != std::string::npos){
        return true;
    }
    else {
        return false;
    }
}

// templates for basic arithmetic operations

template <typename T>
T add(T param_a, T param_b){
    return param_a + param_b;
}

template <typename T>
T subtract(T param_a, T param_b){
    return param_a - param_b;
}

template <typename T>
T multiply(T param_a, T param_b){
    return param_a * param_b;
}

template <typename T>
T divide(T param_a, T param_b){
    return param_a / param_b;
}

// this funcion adds 2 numbers from std::string operation
// and prints the output of the operation
void f_adding(std::string operation){
    // finding the operand and taking from string two numbers
    std::size_t operation_pointer = operation.find('+');
    std::string a_param = operation.substr(0, operation_pointer);
    std::string b_param = operation.substr(operation_pointer + 1);
    double param_a, param_b;

    // testing if they are double and converting them into double
    bool a_double = test_coma(a_param);
    bool b_double = test_coma(b_param);

    if (a_double && b_double) {
        param_a = std::stod(a_param);
        param_b = std::stod(b_param);
    }
    else if (a_double && !b_double) {
        param_a = std::stod(a_param);
        param_b = (double) std::stoi(b_param);
    }
    else if (!a_double && b_double) {
        param_a = (double) std::stoi(a_param);
        param_b = std::stoi(b_param);
    }
    else {
        param_a = (double) std::stoi(a_param);
        param_b = (double) std::stoi(b_param);
    }


    std::cout << "= " << add(param_a, param_b) << "\n";

}

// this function subtracts one number form the other
void f_subtracting(std::string operation){
    // finding the operand and taking from string two numbers
    std::size_t operation_pointer = operation.find('-');
    std::string a_param = operation.substr(0, operation_pointer);
    std::string b_param = operation.substr(operation_pointer + 1);
    double param_a, param_b;

    // testing if they are double and converting them into double
    bool a_double = test_coma(a_param);
    bool b_double = test_coma(b_param);

    if (a_double && b_double) {
        param_a = std::stod(a_param);
        param_b = std::stod(b_param);
    }
    else if (a_double && !b_double) {
        param_a = std::stod(a_param);
        param_b = (double) std::stoi(b_param);
    }
    else if (!a_double && b_double) {
        param_a = (double) std::stoi(a_param);
        param_b = std::stoi(b_param);
    }
    else {
        param_a = (double) std::stoi(a_param);
        param_b = (double) std::stoi(b_param);
    }stod(operation.substr(operation_pointer+1), nullptr);

    std::cout << "= " << subtract(param_a, param_b) << "\n";
}

void f_multipling(std::string operation){
    // finding the operand and taking from string two numbers
    std::size_t operation_pointer = operation.find('*');
    std::string a_param = operation.substr(0, operation_pointer);
    std::string b_param = operation.substr(operation_pointer + 1);
    double param_a, param_b;

    // testing if they are double and converting them into double
    bool a_double = test_coma(a_param);
    bool b_double = test_coma(b_param);

    if (a_double && b_double) {
        param_a = std::stod(a_param);
        param_b = std::stod(b_param);
    }
    else if (a_double && !b_double) {
        param_a = std::stod(a_param);
        param_b = (double) std::stoi(b_param);
    }
    else if (!a_double && b_double) {
        param_a = (double) std::stoi(a_param);
        param_b = std::stoi(b_param);
    }
    else {
        param_a = (double) std::stoi(a_param);
        param_b = (double) std::stoi(b_param);
    }
    std::cout << "= " << multiply(param_a, param_b) << "\n";
}

void f_dividing(std::string operation){
    // finding the operand and taking from string two numbers
    std::size_t operation_pointer = operation.find('/');
    std::string a_param = operation.substr(0, operation_pointer);
    std::string b_param = operation.substr(operation_pointer + 1);
    double param_a, param_b;

    // testing if they are double and converting them into double
    bool a_double = test_coma(a_param);
    bool b_double = test_coma(b_param);

    if (a_double && b_double) {
        param_a = std::stod(a_param);
        param_b = std::stod(b_param);
    }
    else if (a_double && !b_double) {
        param_a = std::stod(a_param);
        param_b = (double) std::stoi(b_param);
    }
    else if (!a_double && b_double) {
        param_a = (double) std::stoi(a_param);
        param_b = std::stoi(b_param);
    }
    else {
        param_a = (double) std::stoi(a_param);
        param_b = (double) std::stoi(b_param);
    }

    if (param_b == 0){
        std::cout << "Error, you can't divide by 0.\n";
    }
    else {
        std::cout << "= " << divide(param_a, param_b) << "\n";
    }
}

// this function tests if there is "exit" in the input
// if there is returns true otherwise it returns false
bool exiting_function(std::string sentence){
    sentence += ' ';
    std::string exit = "exit";
    std::size_t found = sentence.find(exit);

    if (found != std::string::npos){
        sentence = remove_spaces(sentence);
        sentence = replace_comas(sentence);
        return true;
    }
    else {
        sentence = remove_spaces(sentence);
        sentence = replace_comas(sentence);
        return false;
    }

}

// this function tests what opeartion is in input and if it is acceptable by other functions
// than it calls the matching function
// or gives back error if the input is invalid
void test_what_operation(std::string operation){
    bool adding = false;
    bool subtracting = false;
    bool multipling = false;
    bool dividing = false;
    unsigned long operation_length = operation.length();

    for(int i = 0; i < operation_length; i++){
        if (operation[i] == '+'){
            adding = true;
        }
        if (operation[i] == '-'){
            subtracting = true;
        }
        if (operation[i] == '*'){
            multipling = true;
        }
        if (operation[i] == '/'){
            dividing = true;
        }
    }

    if (adding && subtracting){
        std::cout << "Calculator does not allow such operations.";
    }
    else if (adding && multipling){
        std::cout << "Calculator does not allow such operations.";
    }
    else if (adding && dividing){
        std::cout << "Calculator does not allow such operations.";
    }
    else if (subtracting && multipling){
        std::cout << "Calculator does not allow such operations.";
    }
    else if (subtracting && dividing){
        std::cout << "Calculator does not allow such operations.";
    }
    else if (multipling && dividing){
        std::cout << "Calculator does not allow such operations.";
    }
    else if (adding){
        f_adding(operation);
    }
    else if (subtracting){
        f_subtracting(operation);
    }
    else if (multipling){
        f_multipling(operation);
    }
    else if (dividing){
        f_dividing(operation);
    }
}

int main() {
    std::cout << "*** Basic Calculator ***\n" << std::endl;
    std::string input;
    bool exit_flag;

    while(1){
        std::getline(std::cin, input);
        exit_flag = exiting_function(input);
        if (exit_flag){
            break;
        }
        else{
            test_what_operation(input);
        }
    }
    return 0;
}