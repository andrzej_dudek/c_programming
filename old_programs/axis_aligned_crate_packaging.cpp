#include <iostream>

// This are functions for a daily programmer challenge #377 [easy]
// From dailyprogrammer subreddit

/*
 * @param global_x - int value of the magazine length on x axis
 * @param global_y - int value of the magazine length on y axis
 * @param crate_x - int vlaue of the crate length on x axis
 * @param crate_y - int value of the crate length on y axis
 * @return - function returns how many identical crates you can put in the magazine without turning them around
*/
int fit1(int global_x, int global_y, int crate_x, int crate_y){
    int boxes_in_row = global_x / crate_x;
    int boxes_in_column = global_y / crate_y;
    return boxes_in_row * boxes_in_column;
}

/*
* @param global_x - int value of the magazine length on x axis
* @param global_y - int value of the magazine length on y axis
* @param crate_x - int vlaue of the crate length on x axis
* @param crate_y - int value of the crate length on y axis
* @return - function returns how many identical crates you can put in the magazine if you can turn them 90 deegrees
*/
int fit2(int global_x, int global_y, int crate_x, int crate_y){
    int first_result = fit1(global_x, global_y, crate_x, crate_y);
    int turned_result = fit1(global_x, global_y, crate_y, crate_x);
    return first_result >= turned_result ? first_result : turned_result;
    // return first_result if it is bigger than turned_result
    // otherwise return turned_result
}

int main() {
    std::cout << "Testing function fit1:\n";
    std::cout << "fit1(25, 18, 6, 5) => " << fit1(25, 18, 6, 5) << "\n";
    std::cout << "fit1(10, 10, 1, 1) => " << fit1(10, 10, 1, 1) << "\n";
    std::cout << "fit1(12, 34, 5, 6) => " << fit1(12, 34, 5, 6) << "\n";
    std::cout << "fit1(12345, 678910, 1112, 1314) => " << fit1(12345, 678910, 1112, 1314) << "\n";
    std::cout << "fit1(5, 100, 6, 1) => " << fit1(5, 100, 6, 1) << "\n";

    std::cout << "Testing function fit2:\n";
    std::cout << "fit2(25,18,6,5) => " << fit2(25, 18, 6, 5) << "\n";
    std::cout << "fit2(12,34,5,6) => " << fit2(12, 34, 5, 6) << "\n";
    std::cout << "fit2(12345, 678910, 1112, 1314) => " << fit2(12345, 678910, 1112, 1314) << "\n";
    std::cout << "fit2(5,5,3,2) => " << fit2(5, 5, 3, 2) << "\n";
    std::cout << "fit2(5,100,6,1) => " << fit2(5, 100, 6, 1) << "\n";
    std::cout << "fit2(5,5,6,1) => " << fit2(5, 5, 6, 1) << "\n";
}