#include <iostream>

int abs(int a, int b){
    if(a > b) return (a - b);
    else return (b - a);
}

int ducci_seq(int* tab, int size){
    int *help_tab = new int[size];
    for (int i = 0; i < size; i++) {
        if (i == (size - 1)) {
            *(help_tab + i) = abs(*tab, *(tab + i));
        } else {
            *(help_tab + i) = abs(*(tab + i), *(tab + i + 1));
        }
    }

    for (int i = 0; i < size; i++) {
        *(tab + i) = *(help_tab + i);
    }

    delete[] help_tab;

    std::cout << "(";
    for (int i = 0; i < size; i++) {
        if (i == 0) {
            std::cout << *(tab + i) << ",";
        } else if (i == (size - 1)) {
            std::cout << " " << *(tab + i);
        } else {
            std::cout << " " << *(tab + i) << ",";
        }
    }
    std::cout << ")\n";

    int sum = 0;
    for (int i = 0; i < size; i++) {
        sum = *(tab + i);
    }

    return sum;
}

int ducci_seq_master(int *tab, int size){
    int steps = 1;
    while(1){
        steps++;
        if(!ducci_seq(tab, size)){
            break;
        }
    }
    return steps;
}


int main() {
    std::cout << "(0, 653, 1854, 4063)" << std::endl;
    int tab[] = {0,653,1854,4063};
    //int tab[] = {1, 5, 7, 9, 9};
    int steps = ducci_seq_master(tab, 4);
    std::cout << "Steps: " << steps << "\n";
    return 0;
}