#include <iostream>
#include <random>
#include <chrono>

// this function removes spaces from the sentence
// and returns the string without the spaces
std::string remove_spaces(std::string sentence){
    int sentence_length = sentence.length();
    std::string return_string = "";

    for(int i = 0; i<sentence_length; i++){
        if (sentence[i] != ' '){
            return_string += sentence[i];
        }
        else continue;
    }

    return return_string;
}

// this function generates n = parts random numbers [1,range] and returns
// sum of those numbers
int generate_sum_of_random_numbers(int parts , int range){
    int sum = 0;
    // generating seed for a genarator
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    // initializing a generaotor to generate random numbers
    std::default_random_engine generator(seed);
    // using the uniform_int_distribution from 1 to range
    std::uniform_int_distribution<int> distribution(1, range);


    for(int i = 0; i<parts; i++){
        sum += distribution(generator);
    }

    return sum;
}

// this funcion takes a string and test it if it is a roll (has 'd' in it)
// then it counts how many rolls there is and what dice is used to do them
// and returns the score of those throws
int dice_roll(std::string roll){
    roll = remove_spaces(roll);
    int roll_length = roll.length();
    std::string::size_type d_pointer;
    bool d_flag = false;

    for(int i = 0; i < roll_length; i++){
        if(roll[i] == 'd'){
            d_flag = true;
        }
    }

    if(!d_flag){
        std::cout << "Invalid input. Missing \'d\'.\n";
    }

    int rolls = std::stoi(roll, &d_pointer);
    int rolls_range = std::stoi(roll.substr(d_pointer+1), nullptr);

    int score = generate_sum_of_random_numbers(rolls, rolls_range);
    return score;
}

int main() {
    std::cout << "How many rolls:";
    int rolls;
    int max = rolls;
    std::cin >> rolls;
    std::cout << "You have " << rolls << " roll/rolls\n";
    std::string input;
    std::getline(std::cin,input);

    while(rolls != 0){
        std::getline(std::cin, input);
        //std::cin >> input;
        std::cout << dice_roll(input) << "\n";
        rolls--;
    }

    return 0;
}