#include <iostream>

unsigned int count_char(std::string word, unsigned int length, char character){
    unsigned int counter = 0;

    for (int i = 0; i < length; i++){
        if (word[i] == character){
            counter++;
        }
        else continue;
    }
    return counter;
}

bool balanced(std::string word){
    unsigned int word_length = word.length();
    unsigned int y_counter = count_char(word, word_length, 'y');
    unsigned int x_counter = count_char(word, word_length, 'x');

    if (word_length == 0) return true;
    if (x_counter == 0 && y_counter == 0) return false;
    // returns true if x_counter == y_counter otherwise returns false
    return x_counter == y_counter;
}

bool bonus_balanced(std::string word){
    unsigned int word_length = word.length();
    std::string alphabet = "abcdefghijklmnopqrstuvwxyz";
    int alphabet_length = alphabet.length();
    unsigned int char_counter_tab[alphabet_length];
    bool flag = true;

    if (word_length == 0) return true;

    for (int i = 0; i < alphabet_length; i++){
        char_counter_tab[i] = count_char(word, word_length, alphabet[i]);
    }

    unsigned int reference = 0;

    for (int i = 0; i < alphabet_length; i++){
        if (char_counter_tab[i] == 0) continue;
        if (reference != 0 && reference != char_counter_tab[i]) {
            flag = false;
            break;
        }
        reference = char_counter_tab[i];
    }

    return flag;
}

void printer(bool flag){
    if (flag){
        std::cout << "true\n";
    }
    else{
        std::cout << "false\n";
    }
}

int main() {
    int i = 10;
    bool checker;
    std::string input;
    std::cout << "Reddit balanced and balanced_bounus programming challenge\n";

    while(i){
        std::cin >> input;
        checker = balanced(input);
        printer(checker);
        checker = bonus_balanced(input);
        printer(checker);
        i--;
    }
}