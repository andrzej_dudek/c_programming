#include <iostream>

std::string tens[] = {"oh ", "ten ", "twenty ", "thirty ", "fourty ", "fifty "};
std::string from_one_to_twenty[] = {"twelve ", "one ", "two ", "three ", "four ", "five ",
                                    "six ", "seven ", "eight ", "nine ", "ten ", "eleven ", "twelve ",
                                    "thirteen ", "fourteen ", "fifteen ", "sixteen ", "seventeen ",
                                    "eighteen ", "nineteen "};

// input:
// sentence - any string
// output:
// return_string - sentence without spaces
std::string remove_spaces(std::string sentence){
    int sentence_length = sentence.length();
    std::string return_string = "";

    for(int i = 0; i<sentence_length; i++){
        if (sentence[i] != ' '){
            return_string += sentence[i];
        }
        else continue;
    }

    return return_string;
}

// input:
// hours - int number representig hours
// minutes - int number representing minutes
// result:
// prints the hour on the screen using words
void print_clock(int hours, int minutes){
    std::string ampm = "am";

    if (hours >= 12){
        ampm = "pm";
    }

    if (minutes == 0){
        std::cout << "It's " << from_one_to_twenty[hours%12] << ampm << "\n";
    }
    else if (minutes%10 == 0){
        std::cout << "It's " << from_one_to_twenty[hours%12] << tens[minutes/10] << ampm << "\n";
    }
    else if (minutes < 10 || minutes > 20){
        std::cout << "It's " << from_one_to_twenty[hours%12];
        std::cout << tens[minutes/10] << from_one_to_twenty[minutes%10] << ampm << "\n";
    }
    else {
        std::cout << "It's " << from_one_to_twenty[hours%12] << from_one_to_twenty[minutes] << ampm << "\n";
    }

}

// input:
// hour - string which contains the hour
// result:
// printing the hour on the screen using words
void describe_clock(std::string hour){
    hour = remove_spaces(hour);
    std::string::size_type two_dots_pointer;
    int hours_number, minutes_number;

    hours_number = std::stoi(hour, &two_dots_pointer);
    minutes_number = std::stoi(hour.substr(two_dots_pointer+1));

    print_clock(hours_number, minutes_number);

}


int main() {
    int i = 10;
    std::cout << "What's the hour on your clock?:";
    std::string input;

    while(i){
        std::cin >> input;
        describe_clock(input);
        i--;
    }
    return 0;
}